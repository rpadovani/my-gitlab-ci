**Thanks for your interest in My Gitlab CI!** 

This is an open source project, licensed under MIT license (see LICENSE for other information), therefore you can 
contribute as well. There are a lot of different possible tasks, let me illustrate some of them - but feel free to 
[open an issue][1] if there is something else you want to contribute on!

## Suggestions for improvements

Do you have any feedback, compliant, suggestion? Feel free to [open a new issue][1] in [the official repo][0].

If you'd prefer to communicate with me directly, feel free to send me an email at `riccardo@rpadovani.com`, or write 
me on [Telegram (@rpadovani)][2]. Write me also only for saying `Hi!`, I alway reply to everyone and I like to know that
my work is appreciated.

## Designs

Do you like creating user interfaces? Are you an user experience expert? I'd love to know how to improve the project - 
for the moment I just shamelessly copied the style of gitlab.com, with results that are not the best - sorry, UI/UX is 
not strong in me!

### Logo

Speaking of design, the project still doesn't have a logo (it's the [first issue opened!][5]): if you like, create one,
you will have my eternal gratitude and a couple of beers if we will ever meet! 

## Code

The codebase is in [Vue.js][3]: the choice was made because I never used it before and wanted to learn it, 
therefore it is not the best possible codebase you can find in the wild. Anyhow, feel free to take an [existing issue][4] 
and work on it, or to propose your own improvement. I am always open to feedback and suggestions.

## Thanks

Remember to put your name in the [package.json][6] file if you contribute: I will also take in consideration creating a
page with the list of all contributors, and if we ever meet in person I'll offer you a beer (or an Apfelschorle if you 
don't like alcohol). Do you have any better idea? Let [me][7] know :-)


[0]: https://gitlab.com/rpadovani/my-gitlab-ci
[1]: https://gitlab.com/rpadovani/my-gitlab-ci/issues/new
[2]: https://telegram.me/rpadovani
[3]: https://vuejs.org/
[4]: https://gitlab.com/rpadovani/my-gitlab-ci/issues
[5]: https://gitlab.com/rpadovani/my-gitlab-ci/issues/1
[6]: https://docs.npmjs.com/files/package.json#people-fields-author-contributors
[7]: https://rpadovani.com/about
