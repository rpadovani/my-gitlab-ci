# My Gitlab CI

[![pipeline status](https://gitlab.com/rpadovani/my-gitlab-ci/badges/master/pipeline.svg)](https://gitlab.com/rpadovani/my-gitlab-ci/commits/master)

> A control panel for Gitlab CI runners

## Design

Follow the [Gitlab UX guide][0]

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

[0]: https://docs.gitlab.com/ce/development/ux_guide/
