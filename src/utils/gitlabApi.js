import {store} from '../store'

export async function getRunners () {
  const runners = await fetch(`${store.getters.host}/api/v4/runners/`, {
    headers: {
      'Authorization': `Bearer ${store.getters.accessToken}`
    },
    method: 'GET'
  })

  return runners.json()
}

export async function getRunningJobs (runnerId, ignoreFail = false) {
  const runningJob = await fetch(`${store.getters.host}/api/v4/runners/${runnerId}/jobs?status=running`, {
    headers: {
      'Authorization': `Bearer ${store.getters.accessToken}`
    },
    method: 'GET'
  })

  if (!ignoreFail && (runningJob.status < 200 || runningJob.status >= 300)) {
    throw runningJob.statusText
  }

  return runningJob.json()
}

export async function getRunner (runnerId) {
  const runner = await fetch(`${store.getters.host}/api/v4/runners/${runnerId}`, {
    headers: {
      'Authorization': `Bearer ${store.getters.accessToken}`
    },
    method: 'GET'
  })

  if (runner.status < 200 || runner.status >= 300) {
    throw runner.statusText
  }

  return runner.json()
}

export async function getJobs (runnerId, perPage = 20, page = 1) {
  const jobs = await fetch(`${store.getters.host}/api/v4/runners/${runnerId}/jobs?per_page=${perPage}&page=${page}`, {
    headers: {
      'Authorization': `Bearer ${store.getters.accessToken}`
    },
    method: 'GET'
  })

  if (jobs.status < 200 || jobs.status >= 300) {
    throw jobs.statusText
  }

  return {
    jobs: await jobs.json(),
    headers: jobs.headers
  }
}

export async function getUser () {
  const user = await fetch(`${store.getters.host}/api/v4/user`, {
    headers: {
      'Authorization': `Bearer ${store.getters.accessToken}`
    },
    method: 'GET'
  })

  return user.json()
}

export async function getUserProjects (page = 1) {
  const projects = await fetch(`${store.getters.host}/api/v4/projects?membership=true&page=${page}`, {
    headers: {
      'Authorization': `Bearer ${store.getters.accessToken}`
    },
    method: 'GET'
  })

  return {
    projects: await projects.json(),
    headers: projects.headers
  }
}

export async function getRunnersByProject (projectId) {
  const projects = await fetch(`${store.getters.host}/api/v4/projects/${projectId}/runners`, {
    headers: {
      'Authorization': `Bearer ${store.getters.accessToken}`
    },
    method: 'GET'
  })

  return projects.json()
}

export async function getProject (projectId) {
  const project = await fetch(`${store.getters.host}/api/v4/projects/${projectId}`, {
    headers: {
      'Authorization': `Bearer ${store.getters.accessToken}`
    },
    method: 'GET'
  })

  return project.json()
}
