import Vue from 'vue'
import Router from 'vue-router'

import { store } from '@/store'

import RunnersList from '@/components/runnersList/RunnersList'
import SingleRunner from '@/components/singleRunner/SingleRunner'
import Login from '@/components/auth/Login'
import ProjectsList from '@/components/projects/ProjectsList'
import Callback from '@/components/auth/Callback'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'RunnersList',
      component: RunnersList,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: '/projects',
      name: 'ProjectsList',
      component: ProjectsList,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: '/runners/:id',
      name: 'SingleRunner',
      component: SingleRunner,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: '/auth',
      name: 'Login',
      component: Login
    },
    {
      path: '/auth/callback',
      name: 'Callback',
      component: Callback
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresLogin) && !store.getters.accessToken) {
    next('/auth')
  } else {
    next()
  }
})

export default router
