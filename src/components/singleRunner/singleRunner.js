import { getRunner, getJobs } from '@/utils/gitlabApi'
import pageNumbers from '@/commons/PageNumbers'

import JobRow from './JobRow'

async function fetchData (page = 1) {
  try {
    this.loading = true
    this.runner = await getRunner(this.$route.params.id)
  } catch (e) {
    console.error(e)
  } finally {
    this.loading = false
  }

  try {
    this.loadingJobs = true
    const {jobs, headers} = await getJobs(this.$route.params.id, 20, page)

    this.jobs = jobs

    this.pageInfo = {
      perPage: parseInt(headers.get('X-Per-Page'), 10),
      page,
      total: parseInt(headers.get('X-Total'), 10),
      totalPages: parseInt(headers.get('X-Total-Pages'), 10),
      nextPage: parseInt(headers.get('X-Next-Page'), 10),
      previousPage: parseInt(headers.get('X-Prev-Page'), 10)
    }
  } catch (e) {
    if (e === 'Forbidden') {
      this.notAuthJobs = true
    }
    console.error(e)
  } finally {
    this.loadingJobs = false
  }
}

function created () {
  this.fetchData()
}

export const SingleRunner = {
  name: 'singleRunner',

  created,

  data: function () {
    return {
      runner: {},
      jobs: [],
      loading: false,
      loadingJobs: false,
      notAuthJobs: false,
      pageInfo: {}
    }
  },

  methods: {
    fetchData
  },

  components: {
    'job-row': JobRow,
    pageNumbers
  }
}
