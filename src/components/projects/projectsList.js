import { getUserProjects } from '@/utils/gitlabApi'
import pageNumbers from '@/commons/PageNumbers'

async function fetchData (page = 1) {
  try {
    this.loading = true

    const {projects, headers} = await getUserProjects(page)
    this.projects = projects

    this.pageInfo = {
      perPage: headers.get('X-Per-Page'),
      page,
      total: headers.get('X-Total'),
      totalPages: headers.get('X-Total-Pages'),
      nextPage: headers.get('X-Next-Page'),
      previousPage: headers.get('X-Prev-Page')
    }
  } catch (e) {
    console.error(e)
  } finally {
    this.loading = false
  }
}

function created () {
  this.fetchData()
}

export const projectsList = {
  name: 'projectsList',

  created,

  components: {
    pageNumbers
  },

  data: function () {
    return {
      projects: [],
      loading: false,
      pageInfo: {}
    }
  },

  methods: {
    fetchData
  }
}
