import { getRunners, getRunnersByProject, getRunningJobs, getProject } from '@/utils/gitlabApi'

async function fetchData () {
  try {
    this.loading = true

    if (this.$route.query.project) {
      [this.runners, this.pageTitle] = await Promise.all([
        getRunnersByProject(this.$route.query.project),
        getProject(this.$route.query.project)])

      this.pageTitle = 'Runners for ' + this.pageTitle.name
    } else {
      this.runners = await getRunners()
    }

    const currentJobsPromises = []

    for (const runner of this.runners) {
      this.$set(runner, 'currentJob', 'Loading...')

      currentJobsPromises.push(getRunningJobs(runner.id, true))
    }

    this.loading = false

    const currentJobs = await Promise.all(currentJobsPromises)

    this.runners.forEach((runner, index) => {
      let job = 'No job running'

      if (currentJobs[index].message === '403 Forbidden  - No access granted') {
        job = 'No auth'
      } else {
        const currentJob = currentJobs[index][0]  // TODO: plan to support multiple jobs

        if (currentJob) {
          job = `${currentJob.project.name_with_namespace} / ${currentJob.name}`
        }
      }

      runner.currentJob = job
    })
  } catch (e) {
    console.error(e)

    this.loading = false
  }
}

function created () {
  this.fetchData()
}

export const runnersList = {
  name: 'runnersList',

  created,

  data: function () {
    return {
      runners: [],
      loading: false,
      pageTitle: 'Your runners'
    }
  },

  methods: {
    fetchData
  }
}
