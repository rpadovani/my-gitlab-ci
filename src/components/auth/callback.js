import { mapGetters } from 'vuex'
import {SET_ACCESS_TOKEN} from './store'

function splitHash (hash) {
  hash = hash.replace('#', '')

  const params = hash.split('&')

  const returnObject = {}

  for (const param of params) {
    const key = param.split('=')[0]
    returnObject[key] = param.split('=')[1]
  }

  return returnObject
}

function isStateHashValid (incomingState) {
  return true // TODO: we probably need to save the state in the local storage, since we change domain to login
  // return incomingState === this.loginStateHash
}

function mounted () {
  const hash = splitHash(this.$route.hash)

  if (this.isStateHashValid(hash.state)) {
    this.$store.commit(SET_ACCESS_TOKEN, hash.access_token)
    this.$router.push('/')
  }
}

export const callbackComponent = {
  name: 'callback',

  computed: {
    ...mapGetters({
      loginStateHash: 'loginStateHash'
    })
  },

  methods: {
    isStateHashValid
  },

  mounted
}
