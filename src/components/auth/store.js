export const SET_LOGIN_STATE_HASH = 'components/auth/SET_LOGIN_STATE_HASH'
export const SET_ACCESS_TOKEN = 'components/auth/SET_ACCESS_TOKEN'
export const SET_HOST = 'components/auth/SET_HOST'

const state = {
  loginStateHash: null,
  accessToken: null,
  host: null
}

const getters = {
  loginStateHash: state => state.loginStateHash,
  host: state => {
    if (state.host) {
      return state.host
    }

    const localStorageHost = localStorage.getItem('host')

    if (localStorageHost) {
      state.host = localStorageHost

      return localStorageHost
    }

    return null
  },
  accessToken: state => {
    if (state.accessToken) {
      return state.accessToken
    }

    const localStorageAccessToken = localStorage.getItem('accessToken')

    if (localStorageAccessToken) {
      state.accessToken = localStorageAccessToken

      return localStorageAccessToken
    }

    return null
  }
}

const mutations = {
  [SET_LOGIN_STATE_HASH] (state, hash) {
    state.loginStateHash = hash
  },

  [SET_ACCESS_TOKEN] (state, token) {
    localStorage.setItem('accessToken', token)

    state.accessToken = token
  },

  [SET_HOST] (state, host) {
    localStorage.setItem('host', host)

    state.host = host
  }
}

export const authStore = {
  state,
  getters,
  mutations
}
