import {SET_LOGIN_STATE_HASH} from './store'
import {defaultAppId, defaultHost} from '../../../static/config'
import {SET_HOST} from '@/components/auth/store'

const redirectUri = `${document.location.protocol}//${document.location.host}/auth/callback`
const scope = 'api'

function oauthUrl () {
  const stateHash = this.$store.getters.loginStateHash
  let host = this.host

  if (host.substring(0, 4) !== 'http') {
    host = `https://${host}`
  }

  host = host.replace(/\/$/, '')
  this.$store.commit(SET_HOST, host)

  const url = `${host}/oauth/authorize?client_id=${this.appId}&redirect_uri=${redirectUri}&response_type=token&state=${stateHash}&scope=${scope}`

  return encodeURI(url)
}

function mounted () {
  const stateHash = Date.now().toString() // TODO: better hash

  this.$store.commit(SET_LOGIN_STATE_HASH, stateHash)
}

const STEPS = Object.freeze({
  STANDARD_LOGIN: 'STANDARD_LOGIN',
  CUSTOM_LOGIN: 'CUSTOM_LOGIN'
})

export const loginComponent = {
  name: 'auth',

  computed: {
    oauthUrl
  },

  data: function () {
    return {
      step: STEPS.STANDARD_LOGIN,
      steps: STEPS,
      host: defaultHost,
      appId: defaultAppId
    }
  },

  methods: {
    customDomain: function () {
      this.step = STEPS.CUSTOM_LOGIN
    }
  },

  mounted
}
